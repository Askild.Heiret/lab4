package cellular;

import datastructure.IGrid;

public class BriansBrain extends GameOfLife {

    /**
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param rows
     * @param columns
     */
    public BriansBrain(int rows, int columns) {
        super(rows, columns);
    }
    public CellState getNextCell(int row, int col){
        if (getCellState(row, col) == CellState.DEAD) {
            if (countNeighbors(row, col, CellState.ALIVE) == 2) {
                return CellState.ALIVE;
            }
            else{
                return CellState.DEAD;
            }
        }
        if (getCellState(row, col) == CellState.ALIVE){
            return CellState.DYING;
        }
        if (getCellState(row, col) == CellState.DYING){
            return CellState.DEAD;
        }
        return null;
    }
}
