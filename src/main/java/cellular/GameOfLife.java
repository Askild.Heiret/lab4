package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();

		for (int i = 0; i < currentGeneration.numRows(); i++) {
			for (int j = 0; j < currentGeneration.numColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		currentGeneration = nextGeneration;
	}


	@Override
	public CellState getNextCell(int row, int col) {
		if (getCellState(row, col) == CellState.DEAD) {
			if (countNeighbors(row, col, CellState.ALIVE) == 3) {
				return CellState.ALIVE;
			}
			else{
				return CellState.DEAD;
			}
		}
		else if (getCellState(row, col) == CellState.ALIVE){
			if (countNeighbors(row, col, CellState.ALIVE) == 3 || countNeighbors(row, col, CellState.ALIVE) == 2){
				return CellState.ALIVE;
			}
			if (countNeighbors(row, col, CellState.ALIVE) > 3){
				return CellState.DEAD;
			}
			if (countNeighbors(row, col, CellState.ALIVE) < 2){
				return CellState.DEAD;
			}
		}
		return null;
	}

		/**
		 * Calculates the number of neighbors having a given CellState of a cell on
		 * position (row, col) on the board
		 *
		 * Note that a cell has 8 neighbors in total, of which any number between 0 and
		 * 8 can be the given CellState. The exception are cells along the boarders of
		 * the board: these cells have anywhere between 3 neighbors (in the case of a
		 * corner-cell) and 5 neighbors in total.
		 *
		 * @param x     the x-position of the cell
		 * @param y     the y-position of the cell
		 * @param state the Cellstate we want to count occurences of.
		 * @return the number of neighbors with given state
		 */
		public int countNeighbors ( int row, int col, CellState state){

			int count = 0;

			int maxC = numberOfColumns() - 1;
			int maxR = numberOfRows() - 1;

			boolean nWcorner = row == 0 && col == 0;
			boolean sWcorner = row == maxR && col == 0;
			boolean sEcorner = row == maxR && col == maxC;
			boolean nEcorner = row == 0 && col == maxC;

			boolean nEdge = row == 0 && !(nWcorner || nEcorner);
			boolean wEdge = col == 0 && !(nWcorner || sWcorner);
			boolean sEdge = row == maxR && !(sWcorner || sEcorner);
			boolean eEdge = col == maxC && !(sEcorner || nEcorner);

			boolean inner = !(col == 0 || col == maxC || row == 0 || row == maxR);

			if(inner){
				if(getCellState(row - 1, col - 1) == state){//1
					count++;
				}
				if(getCellState(row - 1, col) == state){//2
					count++;
				}
				if(getCellState(row - 1, col + 1) == state){//3
					count++;
				}
				if(getCellState(row, col + 1) == state){//4
					count++;
				}
				if(getCellState(row + 1, col + 1) == state){//5
					count++;
				}
				if(getCellState(row + 1, col) == state){//6
					count++;
				}
				if(getCellState(row + 1, col - 1) == state){//7
					count++;
				}
				if(getCellState(row, col - 1) == state){//8
					count++;
				}
			}
			if (nWcorner){
				if(getCellState(row, col + 1) == state){
					count++;
				}
				if(getCellState(row + 1, col + 1) == state){
					count++;
				}
				if(getCellState(row + 1, col) == state){
					count++;
				}
			}
			if(nEcorner) {
				if (getCellState(row + 1, col) == state) {
					count++;
				}
				if (getCellState(row + 1, col - 1) == state) {
					count++;
				}
				if (getCellState(row, col - 1) == state) {
					count++;
				}
			}
			if(sEcorner) {
				if (getCellState(row, col - 1) == state) {//8
					count++;
				}
				if (getCellState(row - 1, col - 1) == state) {//1
					count++;
				}
				if (getCellState(row - 1, col) == state) {//2
					count++;
				}
			}
			if(sWcorner){
				if(getCellState(row - 1, col) == state){//2
					count++;
				}
				if(getCellState(row - 1, col + 1) == state){//3
					count++;
				}
				if(getCellState(row, col + 1) == state){//4
					count++;
				}
			}
			if(nEdge){
				if(getCellState(row, col + 1) == state){//4
					count++;
				}
				if(getCellState(row + 1, col + 1) == state){//5
					count++;
				}
				if(getCellState(row + 1, col) == state){//6
					count++;
				}
				if(getCellState(row + 1, col - 1) == state){//7
					count++;
				}
				if(getCellState(row, col - 1) == state){//8
					count++;
				}
			}
			if(eEdge){
				if(getCellState(row - 1, col - 1) == state){//1
					count++;
				}
				if(getCellState(row - 1, col) == state){//2
					count++;
				}
				if(getCellState(row + 1, col) == state){//6
					count++;
				}
				if(getCellState(row + 1, col - 1) == state){//7
					count++;
				}
				if(getCellState(row, col - 1) == state){//8
					count++;
				}
			}
			if(sEdge){
				if(getCellState(row - 1, col - 1) == state){//1
					count++;
				}
				if(getCellState(row - 1, col) == state){//2
					count++;
				}
				if(getCellState(row - 1, col + 1) == state){//3
					count++;
				}
				if(getCellState(row, col + 1) == state){//4
					count++;
				}
				if(getCellState(row, col - 1) == state){//8
					count++;
				}
			}
			if(wEdge){
				if(getCellState(row - 1, col) == state){//2
					count++;
				}
				if(getCellState(row - 1, col + 1) == state){//3
					count++;
				}
				if(getCellState(row, col + 1) == state){//4
					count++;
				}
				if(getCellState(row + 1, col + 1) == state){//5
					count++;
				}
				if(getCellState(row + 1, col) == state){//6
					count++;
				}
			}
			return count;
		}

		@Override
		public IGrid getGrid () {
			return currentGeneration;
		}
}
