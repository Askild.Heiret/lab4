package gui;

import cellular.BriansBrain;
import cellular.CellAutomaton;
import cellular.GameOfLife;

public class Main {

	public static void main(String[] args) {
		CellAutomaton ca = new GameOfLife(100,100);
		CellAutomaton ca2 = new BriansBrain(100, 100);
		CellAutomataGUI.run(ca);
		//CellAutomataGUI.run(ca2);
	}

}
