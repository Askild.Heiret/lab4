package datastructure;

import cellular.CellState;



public class CellGrid implements IGrid {

    private int cols;
    private int rows;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {

        this.cols = columns;
        this.rows = rows;
        grid = new CellState[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < cols; column++) {
                grid[row][column] = initialState;
            }

        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(numRows(), numColumns(), CellState.DEAD);
        for (int i = 0; i < numRows(); i++) {
            for (int j = 0; j < numColumns(); j++) {
                newGrid.set(i, j, grid[i][j]);
            }

        }

        return newGrid;
    }
    
}
